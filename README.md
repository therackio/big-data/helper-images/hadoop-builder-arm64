# hadoop-builder-arm64

Helper image that extends Java buildtools container (https://gitlab.com/therackio/build-tools/helper-images/java-buildtools-ci-arm64) with additional prereqs needed to compile Hadoop from source for ARM64.  This includes needed backported patch for protobuf 2.5.

Patch from: https://gist.github.com/liusheng/64aee1b27de037f8b9ccf1873b82c413
