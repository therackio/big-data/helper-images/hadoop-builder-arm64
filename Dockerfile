ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG PROTOBUF_VERSION 3.7.1
ENV PROTOBUF_VERSION=$PROTOBUF_VERSION

RUN \
    set -ex && \
    cd / && \
    apt update && apt install -y unzip git wget software-properties-common build-essential autoconf automake libtool cmake zlib1g-dev pkg-config libssl-dev bzip2 libbz2-dev libjansson-dev fuse libfuse-dev curl make g++ gsasl

RUN \
    set -ex && \
    mkdir -p /protoc && \
    cd /protoc && \
    curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VERSION}/protoc-${PROTOBUF_VERSION}-linux-aarch_64.zip && \
    unzip -q protoc-${PROTOBUF_VERSION}-linux-aarch_64.zip && \
    rm -rf protoc-${PROTOBUF_VERSION}-linux-aarch_64.zip && \
    ls -al && \
    cp -r ./include/google /usr/include/ && \
    ls -al /usr/include/

ENV PATH="/protoc/bin:${PATH}"

RUN protoc --version
